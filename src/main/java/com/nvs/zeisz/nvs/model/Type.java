package com.nvs.zeisz.nvs.model;

public enum Type {
    business, family, friends, hobbies, vital
}
